using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using idolapi.Services;
using AutoMapper;
using idolapi.Controllers;
using idolapi.DB.Models;
using Microsoft.AspNetCore.Mvc;

namespace idolapiTest.Services
{
    [TestFixture]
    public class MoqDeleteIdol : IdolServiceTest
    {
        private readonly Mock<IIdolService> mockIdol = new Mock<IIdolService>();
        private readonly Mock<IMapper> mockMapper = new Mock<IMapper>();

        [Test]
        public async Task DeleteIdol_ReturnActionResult_Return200()
        {
            //Calling Controller using 2 mock Object
            IdolController controller = new IdolController(mockIdol.Object, mockMapper.Object);

            // Setup Services return using Mock
            mockIdol.Setup(x => x.DeleteIdol(1)).ReturnsAsync(1);

            // Get Controller return result
            var actual = await controller.DeleteIdol(1);
            var okResult = actual as ObjectResult;
            
            // Assert result with expected result: this time is 200
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [Test]
        public async Task DeleteIdol_ReturnActionResult_Return400()
        {
            //Calling Controller using 2 mock Object
            IdolController controller = new IdolController(mockIdol.Object, mockMapper.Object);

            // Setup Services return using Mock
            mockIdol.Setup(x => x.DeleteIdol(1)).ReturnsAsync(0);

            // Get Controller return result
            var actual = await controller.DeleteIdol(1);
            var okResult = actual as ObjectResult;

            // Assert result with expected result: this time is 404 Not Found
            Assert.AreEqual(404, okResult.StatusCode);

        }

        List<Idol> idolList = new List<Idol>()
        {
            new Idol {
                IdolId = 1,
                IdolNameJP= "加藤ディーナ",
                IdolNameRomaji= "Katou Dina",
                IdolNameHira= "かとう　ディーナ",
                DOB= "09/11/1990",
                Bust= 82,
                Waist= 52,
                Hip= 84,
                Height= 165,
                ImageURL= "https://javmodel.com/javdata/uploads/dina_katou150.jpg"
            },
            new Idol {
                IdolId = 2,
                IdolNameJP= "星川英智",
                IdolNameRomaji= "Hoshikawa Eichi",
                IdolNameHira= "ほしかわ　えいち",
                DOB= "08/04/1992",
                Bust= 80,
                Waist= 60,
                Hip= 87,
                Height= 157,
                ImageURL= "https://javmodel.com/javdata/uploads/eichi_hoshikawa150.jpg"
            },
            new Idol {
                IdolId = 3,
                IdolNameJP= "双葉ひより",
                IdolNameRomaji= "Hiyori Futaba",
                IdolNameHira= "かとう　ディーナ",
                DOB= "03/24/2000",
                Bust= 92,
                Waist= 50,
                Hip= 88,
                Height= 165,
                ImageURL= "https://javmodel.com/javdata/uploads/futaba-hiyori150.jpg"
            },
            new Idol{
                IdolId = 4,
                IdolNameJP= "麻生遥",
                IdolNameRomaji= "Aso Haruka",
                IdolNameHira= "あそはるか",
                DOB= "6/28/1995",
                Bust= 84,
                Waist= 59,
                Hip= 88,
                Height= 160,
                ImageURL= "https://javmodel.com/javdata/uploads/haruka_aso150.jpg"
            },
            new Idol {
                IdolId = 5,
                IdolNameJP= "浅倉愛",
                IdolNameRomaji= "Asakura Ai",
                IdolNameHira= "あさくら　あい",
                DOB= "10/28/1993",
                Bust= 83,
                Waist= 60,
                Hip= 83,
                Height= 170,
                ImageURL= "https://javmodel.com/javdata/uploads/ai_asakura150.jpg"
            }
        };

    }
}