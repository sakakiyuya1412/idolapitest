# This project is used for testing Idol Web API

## Setup instruction

> Follow this instruction to setup your test project with your Web API project

- Go to outside of your Web API project folder type:

```
dotnet new sln -o idol
cd idolapi
```

- Copy your Web API project to inside the Solution **idol**
- Clone this project inside of **idol** too
- After cloned, follow this step:

```
dotnet sln add idolapi/idolapi.csproj
dotnet sln add idolapiTest/idolapiTest.csproj
cd idolapiTest
dotnet add reference ../idolapi/idolapi.csproj
```

