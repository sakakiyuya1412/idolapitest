using System.Collections.Generic;
using idolapi.DB;
using idolapi.DB.Models;
using idolapi.Services;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Moq;

namespace idolapiTest
{
    [TestFixture]
    public class IdolServiceTest
    {
        protected DataContext _context;
        protected IIdolService _idolService;
        List<Idol> idolList = new List<Idol>()
        {
            new Idol {
                IdolId = 1,
                IdolNameJP= "加藤ディーナ",
                IdolNameRomaji= "Katou Dina",
                IdolNameHira= "かとう　ディーナ",
                DOB= "09/11/1990",
                Bust= 82,
                Waist= 52,
                Hip= 84,
                Height= 165,
                ImageURL= "https://javmodel.com/javdata/uploads/dina_katou150.jpg"
            },
            new Idol {
                IdolId = 2,
                IdolNameJP= "星川英智",
                IdolNameRomaji= "Hoshikawa Eichi",
                IdolNameHira= "ほしかわ　えいち",
                DOB= "08/04/1992",
                Bust= 80,
                Waist= 60,
                Hip= 87,
                Height= 157,
                ImageURL= "https://javmodel.com/javdata/uploads/eichi_hoshikawa150.jpg"
            },
            new Idol {
                IdolId = 3,
                IdolNameJP= "双葉ひより",
                IdolNameRomaji= "Hiyori Futaba",
                IdolNameHira= "かとう　ディーナ",
                DOB= "03/24/2000",
                Bust= 92,
                Waist= 50,
                Hip= 88,
                Height= 165,
                ImageURL= "https://javmodel.com/javdata/uploads/futaba-hiyori150.jpg"
            },
            new Idol{
                IdolId = 4,
                IdolNameJP= "麻生遥",
                IdolNameRomaji= "Aso Haruka",
                IdolNameHira= "あそはるか",
                DOB= "6/28/1995",
                Bust= 84,
                Waist= 59,
                Hip= 88,
                Height= 160,
                ImageURL= "https://javmodel.com/javdata/uploads/haruka_aso150.jpg"
            },
            new Idol {
                IdolId = 5,
                IdolNameJP= "浅倉愛",
                IdolNameRomaji= "Asakura Ai",
                IdolNameHira= "あさくら　あい",
                DOB= "10/28/1993",
                Bust= 83,
                Waist= 60,
                Hip= 83,
                Height= 170,
                ImageURL= "https://javmodel.com/javdata/uploads/ai_asakura150.jpg"
            }
        };

        [SetUp]
        public void Setup()
        {
            //Create option for Fake DB
            var option = new DbContextOptionsBuilder<DataContext>()
            .UseInMemoryDatabase(databaseName: "idolapi").Options;
            
            //Add Fake DB to context, service
            _context = new DataContext(option);
            _idolService = new IdolService(_context);

            // //Add Fake data to Context
            _context.Idols.AddRange(idolList);
            _context.SaveChanges();
        }
        
        [TearDown]
        public void TearDown()
        {
            _context.Idols.RemoveRange(_context.Idols);
            _context.SaveChanges();
        }
    }
}