using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace idolapiTest.Services.IdolService 
{
    [TestFixture]
    public class DeleteIdol : IdolServiceTest
    {
        public static IEnumerable<TestCaseData> DeleteIdolTestCaseTrue
        {
            get {
                yield return new TestCaseData(
                    2
                );
                yield return new TestCaseData(
                    3
                );
            }
        }

        [Test]
        [TestCaseSource("DeleteIdolTestCaseTrue")]
        public async Task DeleteIdolTestTrue(int idolId)
        {
            // Arrange in Testcase Source

            // Act 
            var rs = await _idolService.DeleteIdol(idolId);

            //Assert
            Assert.That(rs, Is.EqualTo(1));
        }
        public static IEnumerable<TestCaseData> DeleteIdolTestCaseFalse
        {
            get {
                yield return new TestCaseData(
                    // If Input is null
                    null
                );
                yield return new TestCaseData(
                    // If input is not in DB
                    100
                );
            }
        }
        [Test]
        [TestCaseSource("DeleteIdolTestCaseFalse")]
        public async Task DeleteIdolTestFalse(int idolId)
        {
            // Arrange in Testcase Source

            // Act 
            var rs = await _idolService.DeleteIdol(idolId);

            //Assert
            Assert.That(rs, Is.EqualTo(0));
        }

    }
}